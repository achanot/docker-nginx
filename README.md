
Get Nginx docker image:
```
docker pull nginx
```
Run as example:
```
docker run --name docker-nginx -p 80:80 -d nginx
```
Check that the server is running:
```
docker ps
```

Stop it:
```
docker stop docker-nginx
```

Delete current instance:
```
docker rm docker-nginx
```

Share the config file between Host and Docker and also the webserver root folder:
```
docker run --name docker-nginx -p 80:80 -d -v /home/achanot/71-WebServer/html:/usr/share/nginx/html -v /home/achanot/71-WebServer/nginx.conf:/etc/nginx/nginx.conf:ro nginx
```

You can now access your webserver file you put in /home/achanot/71-WebServer/html at the address http://localhost/

